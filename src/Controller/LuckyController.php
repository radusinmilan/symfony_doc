<?php


namespace App\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LuckyController extends AbstractController
{
    public function number() : Response
    {
        $number = rand(0,100);
        //return $this->render('<html><body> Lucky numer is ' .$number. ' </body></html>');
        return new Response('<html><body> Lucky numer is ' .$number. ' </body></html>');
    }
}